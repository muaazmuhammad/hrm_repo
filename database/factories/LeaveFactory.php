<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Leave;
use Faker\Generator as Faker;

$factory->define(Leave::class, function (Faker $faker) {
    return [
        //
        'comment' => $faker->sentence,
        'count' => $faker->randomDigit,
        'user_id' => factory(User::class)->create()->id,
        'status'=>'pending',
        'start_date' => '2020-11-24',
        'end_date' => '2020-11-27',
        'type'=>'sick'
    ];
});
