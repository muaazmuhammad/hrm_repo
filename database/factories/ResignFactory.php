<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Resign;
use Faker\Generator as Faker;

$factory->define(Resign::class, function (Faker $faker) {
    return [
        //
        'comment' => $faker->sentence,
        'user_id' => factory(User::class)->create()->id,
        'status' => 'pending',
    ];
});
