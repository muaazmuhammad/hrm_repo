<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
    return [
        //
        'comment' => $faker->sentence,
        'user_id' => factory(User::class)->create()->id,
        'date' => date('Y-m-d H:i:s'),
        'type'=>'1'
    ];
});
