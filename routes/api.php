<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'api\ApiController@login');

Route::group(['middleware' => 'auth.jwt'], function () {

    //route for both employee and hr portal  
    Route::get('employee/{user}', 'api\userController@show');              
    Route::get('employeereviews/{user}', 'api\reviewController@index');     
    Route::get('reviews/{id}', 'api\reviewController@show');
    Route::get('employeeAllLeaves/{id}', 'api\leaveController@employeeAllLeaves');               
    Route::get('leaves/{id}', 'api\leaveController@show');
    Route::delete('leaves/{id}', 'api\leaveController@destroy');
    Route::get('logout', 'api\ApiController@logout');
    Route::get('user', 'api\ApiController@getAuthUser');
    Route::get('me', 'api\ApiController@getAuthUser');    //dublicte
    Route::post('changepassword', 'api\userController@changePassword');

    //route for employee portal only
    Route::group(['middleware' => 'employeeCheck'], function () {
        Route::post('leaves', 'api\leaveController@store');
        Route::put('leaves/{leave}', 'api\leaveController@update');
        Route::post('resign', 'api\resignController@store');
        Route::get('employeeresign', 'api\resignController@empAllResign');
        Route::put('resign/{id}', 'api\resignController@update');
        Route::delete('resign/{id}', 'api\resignController@destroy');
   
    });

    //routes for hr portal only 
    Route::group(['middleware' => 'hrCheck'], function () {
        Route::get('employee', 'api\userController@index');
        Route::get('nonactiveemployees', 'api\userController@nonActiveEmployees');
        Route::post('employee', 'api\userController@store');
        Route::put('employee/{user}', 'api\userController@update');
        Route::delete('employee/{user}', 'api\userController@destroy');
        Route::get('allreviews', 'api\reviewController@getAllReviews'); 
        Route::post('reviews', 'api\reviewController@store');
        Route::put('reviews/{review}', 'api\reviewController@update');
        Route::delete('reviews/{review}', 'api\reviewController@destroy');
        Route::get('allResigns', 'api\resignController@showAllResigns');
        Route::get('allleaves', 'api\leaveController@getAllLeaves'); 
        Route::get('approveleave/{leave}','api\leaveController@approveleave'); 
        Route::get('rejectleave/{leave}','api\leaveController@rejectleave'); 
        Route::get('approveresign/{resign}','api\resignController@approveresign'); 
        Route::get('rejectresign/{resign}','api\resignController@RejectResign'); 
        // Route::get('terminate/{id}', 'api\resignController@terminate'); 
        Route::post('terminate', 'api\resignController@terminate'); 
        Route::get('departments', 'api\departmentController@index');
        Route::post('departments', 'api\departmentController@store');
        Route::put('departments/{id}', 'api\departmentController@update');
        Route::delete('departments/{id}', 'api\departmentController@destroy');
        Route::post('sendmail', 'api\resignController@sendNoticeEmail'); 
    });

});

