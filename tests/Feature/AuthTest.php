<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{

    use WithFaker, RefreshDatabase;
    private $testPassword;
    public static $authUser;
    public static $accessToken;
    public $email ;

    protected function setUp(): void
    {
        parent::setUp();
        // $this->testPassword = '12341234';
        // $this->email  = 'muaaz@123.com';  //test email (hr)
    }

    public function testLogin()
    {
        $user = factory(User::class)->create()->toArray();
        // dd($user);
        $this->withoutExceptionHandling();
        $response = $this->json('POST', '/api/login',[
            'email' => $user['email'],
            'password' => 'password'
        ]);
        $response->assertStatus(200);
        self::$accessToken = "Bearer ".$response['token'];
        return $response->decodeResponseJson();
    }
  
     public function testMe(){
        $user = factory(User::class)->create();
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => self::$accessToken,
        ])->json('GET', '/api/me');

        $response->assertStatus(200);
    }

}
