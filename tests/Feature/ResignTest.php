<?php

namespace Tests\Feature;

use App\User;
use App\Resign;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResignTest extends TestCase
{
    use RefreshDatabase;
    private $response;
   

    protected function setUp(): void
    {
        parent::setUp();
        $this->response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => AuthTest::$accessToken,
        ]);
    }

    //test hr get all reviews
    public function testIndex(){
        $this->withoutExceptionHandling();
        $response = $this->actingAs(factory(User::class)->create())->response->json('GET', '/api/allResigns');
        $response->assertStatus(200);
       
    }

    // test hr can add review
    public function testStore(){
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['is_hr' => '0']);
        $resign = factory(Resign::class)->make(['user_id'=>$user->id])->toArray();
        $response = $this->actingAs($user)->response->json('POST', '/api/resign', $resign);
        $response->assertStatus(201);
        }


    // test hr can approve resign
    public function testApproveResign(){
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['is_hr' => '1']);
        $resign = factory(Resign::class)->create()->toArray();
        $response = $this->actingAs($user)->response->json('GET', '/api/approveresign/'.$resign['id']);
        $response->assertStatus(200);
        }

    // test hr can approve resign
    public function testRejectResign(){
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['is_hr' => '1']);
        $resign = factory(Resign::class)->create()->toArray();
        $response = $this->actingAs($user)->response->json('GET', '/api/rejectresign/'.$resign['id']);
        $response->assertStatus(200);
        }

        // test hr can terminate
        public function testTerminate(){
            $this->withoutExceptionHandling();
            $user = factory(User::class)->create(['is_hr' => '1']);
            $emp = factory(User::class)->create(['is_hr' => '0']);
            // $resign = factory(Resign::class)->create()->toArray();
            // dd($emp->id);
            $data= [ 'comment'=>'test comment',  'name'=>$emp->name,'user_id'=>$emp->id,'email'=>$emp->email ];
            $response = $this->actingAs($user)->response->json('POST', '/api/terminate/',$data);
            $response->assertStatus(200);
            }



    //  //test hr can update review       //remaining
    //  public function testUpdate(){
    //     $this->withoutExceptionHandling();
    //     $user = factory(User::class)->create(['is_hr' => '0']);
    //     $resign = factory(Resign::class)->create(['user_id'=>$user->id])->toArray();
    //     $response = $this->actingAs($user)->response->json('PUT', '/api/resign/'.$resign['id'], $resign);
    //     $response->assertStatus(200);
    // }


    //test hr or employee can view
    // public function testShow(){
    //     $this->withoutExceptionHandling();
    //     $resign = factory(Resign::class)->create()->toArray();
    //     $response = $this->actingAs(factory(User::class)->create())->response->json('GET', '/api/resigns/'.$resign['id']);
    //     $response->assertStatus(200);
    // }

  
    //test hr can delete review 
    // public function testDelete(){
    //    $this->withoutExceptionHandling();
    //     $user = factory(User::class)->create(['is_hr' => '0']);  
    //     // dd($user);
    //     $resign = factory(Resign::class)->create(['user_id'=>$user->id])->toArray();
    //     // dd($resign);
    //     $response = $this->actingAs($user)->response->json('DELETE', '/api/resign/'.$resign['id']);
    //     $response->assertStatus(200);
    // }

}
