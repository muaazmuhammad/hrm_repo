<?php

namespace Tests\Feature;

use App\User;
use App\Leave;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LeaveTest extends TestCase
{
    use RefreshDatabase;
    private $response;
   

    protected function setUp(): void
    {
        parent::setUp();
        $this->response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => AuthTest::$accessToken,
        ]);
    }

    // test hr get all leaves
    public function testIndex(){
        $response = $this->actingAs(factory(User::class)->create())->response->json('GET', '/api/allleaves');
        $response->assertStatus(200);
       
    }

    //test employee can add leave
    public function testStore(){
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['is_hr' => '0']);
        $leave = factory(Leave::class)->make(['user_id'=>$user->id])->toArray();
        $response = $this->actingAs($user)->response->json('POST', '/api/leaves', $leave);
        $response->assertStatus(201);
        }


     //test employee can update leave    
     public function testUpdate(){
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['is_hr' => '0']);
        $leave = factory(Leave::class)->create(['user_id'=>$user->id])->toArray();
        $response = $this->actingAs($user)->response->json('PUT', '/api/leaves/'.$leave['id'], $leave);
        $response->assertStatus(200);
    }


    // public function testShow(){
    //     $this->withoutExceptionHandling();
    //     $leave = factory(leave::class)->create()->toArray();
    //     $response = $this->actingAs(factory(User::class)->create())->response->json('GET', '/api/leaves/'.$leave['id']);
    //     $response->assertStatus(200);
    // }

  
    //test hr can delete leave 
    public function testDelete(){
        $user = factory(User::class)->create(['is_hr' => '1']);   //only hr can delete  
        $leave = factory(Leave::class)->create(['user_id'=>$user->id])->toArray();
        $response = $this->actingAs($user)->response->json('DELETE', '/api/leaves/'.$leave['id']);
        $response->assertStatus(200);
    }

    //test hr can approve  leave 
    public function testApproveLeave(){
        $user = factory(User::class)->create(['is_hr' => '1']);    
        $leave = factory(Leave::class)->create(['user_id'=>$user->id])->toArray();
        $response = $this->actingAs($user)->response->json('get', '/api/approveleave/'.$leave['id']);
        $response->assertStatus(200);
    }

      //test hr can reject  leave 
      public function testRejectLeave(){
        $user = factory(User::class)->create(['is_hr' => '1']);    
        $leave = factory(Leave::class)->create(['user_id'=>$user->id])->toArray();
        $response = $this->actingAs($user)->response->json('get', '/api/rejectleave/'.$leave['id']);
        $response->assertStatus(200);
    }
}
