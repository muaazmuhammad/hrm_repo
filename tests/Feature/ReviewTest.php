<?php

namespace Tests\Feature;

use App\User;
use App\Review;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReviewTest extends TestCase
{
    use RefreshDatabase;
    private $response;
   

    protected function setUp(): void
    {
        parent::setUp();
        $this->response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => AuthTest::$accessToken,
        ]);
    }

    //test hr get all reviews
    public function testIndex(){
        $this->withoutExceptionHandling();
        $response = $this->actingAs(factory(User::class)->create())->response->json('GET', '/api/allreviews');
        $response->assertStatus(200);
       
    }

    // test hr can add review
    public function testStore(){
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['is_hr' => '1']);
        $review = factory(Review::class)->create(['user_id'=>$user->id])->toArray();
        $response = $this->actingAs($user)->response->json('POST', '/api/reviews', $review);
        $response->assertStatus(201);
        }


    //  //test hr can update review   
     public function testUpdate(){
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['is_hr' => '1']);
        $review = factory(Review::class)->create(['user_id'=>$user->id])->toArray();
        $response = $this->actingAs($user)->response->json('PUT', '/api/reviews/'.$review['id'], $review);
        $response->assertStatus(200);
    }


    //test hr or employee can view
    public function testShow(){
        $this->withoutExceptionHandling();
        $review = factory(Review::class)->create()->toArray();
        $response = $this->actingAs(factory(User::class)->create())->response->json('GET', '/api/reviews/'.$review['id']);
        $response->assertStatus(200);
    }

  
    //test hr can delete review 
    public function testDelete(){
        $user = factory(User::class)->create(['is_hr' => '1']);   //only hr can delete  
        $review = factory(Review::class)->create(['user_id'=>$user->id])->toArray();
        $response = $this->actingAs($user)->response->json('DELETE', '/api/reviews/'.$review['id']);
        $response->assertStatus(200);
    }


}
