<?php

namespace Tests\Feature;

use App\User;
use App\Department;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;



class DepartmentTest extends TestCase
{

    use RefreshDatabase;
    private $response;
   

    protected function setUp(): void
    {
        parent::setUp();
        $this->response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => AuthTest::$accessToken,
        ]);
    }

    public function testIndex(){
        $response = $this->actingAs(factory(User::class)->create())->response->json('GET', '/api/departments');
        $response->assertStatus(200);
       
    }

    public function testStore(){
        // dd($this->response);
        $this->withoutExceptionHandling();
        $department = factory(Department::class)->make()->toArray();
        $response = $this->actingAs(factory(User::class)->create())->response->json('POST', '/api/departments', $department);
        $response->assertStatus(201);
        return $response->decodeResponseJson();
    }

    //  /**
    //  * @depends testStore
    //  */
     public function testUpdate(){
        $department = factory(Department::class)->create()->toArray();
        // dd($department);
        $response = $this->actingAs(factory(User::class)->create())->response->json('PUT', '/api/departments/'.$department['id'], $department);
        $response->assertStatus(200);
    }
    // /**
    //  * @depends testStore
    //  */
    // public function testShow(){
    //     $this->withoutExceptionHandling();
    //     $department = factory(Department::class)->create()->toArray();
    //     $response = $this->actingAs(factory(User::class)->create())->response->json('GET', '/api/departments/'.$department['id']);
    //     $response->assertStatus(200);
    // }

  
    // /**
    //  * @depends testStore
    //  */
    public function testDelete(){
        $department = factory(Department::class)->create()->toArray();
        $response = $this->actingAs(factory(User::class)->create())->response->json('DELETE', '/api/departments/'.$department['id']);
        $response->assertStatus(200);
    }

}
