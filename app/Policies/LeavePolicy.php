<?php

namespace App\Policies;

use App\Leave;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LeavePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    
    }


    public function viewall(User $user, User $model)  //not used
    {
        //
        return $user->id === $model->id ;
    
    }


    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return mixed
     */
    public function view(User $user, Leave $leave)
    {
        //
        return $user->id === $leave->user_id ;
    
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return mixed
     */
    public function update(User $user, Leave $leave)
    {
        //
        return $user->id == $leave->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return mixed
     */
    public function delete(User $user, Leave $leave)
    {
        //
        return $user->id === $leave->user_id || $user->is_hr=='1';
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return mixed
     */
    public function restore(User $user, Leave $leave)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Leave  $leave
     * @return mixed
     */
    public function forceDelete(User $user, Leave $leave)
    {
        //
    }

}
