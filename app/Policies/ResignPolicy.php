<?php

namespace App\Policies;

use App\Resign;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ResignPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Resign  $resign
     * @return mixed
     */
    public function view(User $user, Resign $resign)
    {
        //
        return $user->id === $resign->user_id ;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Resign  $resign
     * @return mixed
     */
    public function update(User $user, Resign $resign)
    {
        //
        return $user->id === $resign->user_id ;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Resign  $resign
     * @return mixed
     */
    public function delete(User $user, Resign $resign)
    {
        //
        return $user->id === $resign->user_id ;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Resign  $resign
     * @return mixed
     */
    public function restore(User $user, Resign $resign)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Resign  $resign
     * @return mixed
     */
    public function forceDelete(User $user, Resign $resign)
    {
        //
    }
}
