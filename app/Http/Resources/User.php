<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'address' => $this->address,
            'city' => $this->city,
            'country' => $this->country,
            'is_hr' => $this->is_hr,
            'is_terminated' => $this->is_terminated,
            'is_resigned' => $this->is_resigned,
            'joining_date' => $this->created_at,
            "designation"=> $this->designation,
            "salary" => $this->salary,
            "photo" => $this->photo,
            "department_id" => $this->department_id,
            "department" => $this->department->name,
            "updated_at"=> $this->updated_at,
            "created_at"=> $this->created_at,
          
        ];
    }
}
