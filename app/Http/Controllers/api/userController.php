<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Mail\employeeAdded;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\User as UserResource;
use App\Http\Controllers\api\traits\uploadImage;



class userController extends Controller
{

    use uploadImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //hr  get all employees 
        $users = User::where('is_hr',0)->Where('is_terminated',0)->Where('is_resigned',0)->get();
        return response()->json(UserResource::collection($users));
    }


    public function nonActiveEmployees()
    {
        // all non active employees 
        $users = User::where('is_resigned',1)->orWhere('is_terminated',1)->get();
        return response()->json(UserResource::collection($users));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //testing master

        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         //hr can store employees record in user lable  
// return ($request->file('file')->getClientOriginalName());
         //validation
         $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'email' => 'required',
            'password' => 'required',
            'city' => 'required',
            'country' => 'required',
            'designation' => 'required',
            'salary' => 'required',
            
        ]);
    
        $employee = new User();

        // return ($request->image);
        $employee->name = $request->name;
        $employee->phone = $request->phone;
        $employee->address = $request->address;
        $employee->email = $request->email;
        $employee->city = $request->city;
        $employee->country = $request->country;
        $employee->designation = $request->designation;
        $employee->salary = $request->salary;
        $employee->department_id = $request->department_id;
        $employee->password = bcrypt($request->password);
        $pass = $request->password; //for change password api
        if($request->image){
        uploadImage::uploadFile($employee, $request->image);  //trait
        }

        //not used 
        //check if request has a file and store it in storage and its name in user table (photo col)
        // if ($request->hasfile('file'))
        // {
        //     // return 'file agi ';
        //         $file = $request->file('file');
        //         $file->move('storage/employee',$file->getClientOriginalName());  
        //         $employee->photo = $file->getClientOriginalName();
        // }

        // if (!$request->hasfile('file'))
        // {
        //     //   return 'file bhaje chwal insan ';
        // }
  
        //save and send response
        if ($employee->save()) {
            Mail::to($employee->email)
            ->send(new employeeAdded($employee,$pass));
            return response()->json([
                'success' => true,
                'employe'=> new UserResource($employee),   
            ],201);
        }
        else {
            return response()->json([
                'success' => false,
                'message' => ' employee could not be added'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $employee = $user;
        if (!$employee) {
            return response()->json([
                'success' => false,
                'message' => ' employee with id ' . $id . ' cannot be found'
            ], 400);
        }
       
        $this->authorize('view', $employee);

        return response()->json(UserResource::make($employee->load([
                                    'leaves',
                                    'reviews',
                                    'resign',
                                ])));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
       $input = $request->all();

      if($request->image){
        //delete old photo
        $fileName = Str::after($user->photo,'http://10.28.86.195:8000/storage/employee/');
        Storage::disk('local')->delete($fileName);
        uploadImage::uploadFile($user, $request->image);
      }

        $employee = $user;
        if (is_null($employee)){
            return response()->json("employee you want to update is not exist " , 404);
        }
     
       if( $employee->update($input)){
        return response()->json(
            [   'success' => true,
                'message' => 'employee updated',
            ] , 200);
        }
        else {
            return response()->json("not updated" , 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //ht can delete employees
        $employee = $user;
        if (is_null($employee)){
            return response()->json("not found" , 404);
        }
        $employee->delete();
        return response()->json("item is deleted " , 200);
    }

    public function changePassword(Request $request)
    {

        //user can change his/her password
        $input = $request->all();
        $employee = Auth::user();
        if (is_null($employee)){
            return response()->json("employee you want to update is not exist " , 404);
        }

            $hasher = app('hash');
            if ($hasher->check($request->oldPassword, $employee->password)) {
                // Success
                $employee->password = bcrypt($request->password);
                if ($employee->save()) {
                    return response()->json(['success' => 'password changed',],201);
                }
            }
        else {
            return response()->json([ 'success' => false,'message' => 'Please enter correct current password'], 500);
        }
    }

}
