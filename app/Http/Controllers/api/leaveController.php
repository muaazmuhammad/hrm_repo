<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Leave;
use App\Mail\laveRejected;
use App\Mail\leaveApproved;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\Leave as LeaveResource;


class leaveController extends Controller
{

    

    public function getAllLeaves()
    {
        //hr get all leaves of all employees (and can approve or cancel) 
        $leaves = Leave::orderBy('updated_at', 'desc')->get();
             return response()->json([
            'leaves'=>LeaveResource::collection($leaves)
            ]);
    }
    

    public function employeeAllLeaves($id)   //changed
    {
       //an employee get all leaves he applied(created) for  
       if(auth()->user()->id == $id || auth()->user()->is_hr =='1' )
       {
        $leaves = Leave::where('user_id',$id)->orderBy('updated_at', 'desc')->get();
        return response()->json($leaves);
       }
       else {
           return 'you are unauthorized';
       }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'user_id' => 'required|integer',
            'comment' => 'required',
        ]);

        $leave = new Leave();
        $leave->start_date = $request->start_date;
        $leave->end_date = $request->end_date;
        $leave->user_id = $request->user_id;
        $leave->comment = $request->comment;
        $leave->count =  $request->count;   
        $leave->type =  $request->type;   
    
        // dd(Leave::all());
        // check if employee has already applied for the leave 
        $leaveAlreadyExist= Leave::where([['user_id', '=',$request->user_id],['start_date','<=',$request->start_date],['end_date','>=',$request->start_date]])
                        ->orWhere([['user_id', '=',$request->user_id],['start_date','<=',$request->end_date],['end_date','>=',$request->end_date]])
                        ->get();
       
        if ( $leaveAlreadyExist->isEmpty() && $leave->save()) {
            return response()->json([
                'success' => true,
                'leave' => $leave
            ],201);
        }
        else {
            return response()->json([
                'success' => false,
                'message' => 'one of your start date or end date is already exist'
            ], 403);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        $leave = Leave::find($id);
        $this->authorize('view', $leave);
        if (!$leave) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, leave with id ' . $id . ' cannot be found'
            ], 400);
        }
        return $leave;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $leave = Leave::find($id);
        $this->authorize('update', $leave);         //policy
        if (is_null($leave)){
            return response()->json("leave you want to update is not exist " , 404);
        }
        $leave->update($request->all());
        return response()->json(['success' => true, 'message' => 'leave updated',] , 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $leave = Leave::find($id);
        $this->authorize('delete', $leave);

        if (is_null($leave)){
            return response()->json("not found" , 404);
        }
        $leave->delete();
        return response()->json("item is deleted " , 200);
    }

    public function approveleave(Leave $leave){
        // $leave = Leave::find($id);
        $leave->status = 'approved';
        $leave->save();
        Mail::to($leave->user->email)
        ->send(new leaveApproved($leave));
        return response()->json("status changed " , 200);

    }

    public function rejectleave(Leave $leave){
        // $leave = Leave::find($id);
        $leave->status = 'rejected';
        $leave->save();
        Mail::to($leave->user->email)
        ->send(new laveRejected($leave));
        return response()->json("status changed " , 200);
    }
}