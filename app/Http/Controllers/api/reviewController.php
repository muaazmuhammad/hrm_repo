<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class reviewController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  int $user
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {


    
         //an employee get all reviews (created by hr for him) 
         if(auth()->user()->id==$user->id || auth()->user()->is_hr=='1')
         {
            $reviews = Review::where('user_id',$user->id)->get();
            return response()->json($reviews);
         }
         else {
            return response()->json(['message'=>'you are not allowed to access this area'],403);
         }
    }

    
    public function getAllReviews()
    {
         //an hr get all reviews (which he created  for all employees)
         $reviews = Review::all();
         return response()->json($reviews);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //hr can store reviews in review lable
         $this->validate($request, [
            'user_id' => 'required|integer',
            'type' => 'required',
            'comment' => 'required',
            'date' => 'required',
        ]);

        $review = new Review();
        $review->user_id =  $request->user_id;   //user_id of selected  employee
        $review->type = $request->type;
        $review->comment = $request->comment;
        $review->date = $request->date;

        if ($review->save()) {
            return response()->json([
                'success' => true,
                'review' => $review
            ],201);
        }
        else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, review could not be added'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = Review::find($id);
        $this->authorize('view', $review);
        if (!$review) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, review with id ' . $id . ' cannot be found'
            ], 400);
        }
        return $review;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $review = Review::find($id);
        if (is_null($review)){
            return response()->json("review you want to update is not exist " , 404);
        }
        $review->update($request->all());

        return response()->json(
            [   'success' => true,
                'message' => 'review updated',
            ] , 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $review = Review::find($id);
        $this->authorize('delete', $review);
        if (is_null($review)){
            return response()->json("not found" , 404);
        }
        $review->delete();
        return response()->json("item is deleted " , 200);
    }


    public function test()
    {
        //
       
        return 'i am herer  ';
    }
}
