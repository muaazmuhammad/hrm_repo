<?php

namespace App\Http\Controllers\api\traits;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;



trait uploadImage
{
    
    public static function uploadFile(User $user , $base64file)
    {
        
        // dd('hello');
        //base 64  to file 
        $image_64 = $base64file; //your base64 encoded data
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
        $name = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[0]; 
        $replace = substr($image_64, 0, strpos($image_64, ',')+1); 
        
        // find substring fro replace here eg: data:image/png;base64,
        $image = str_replace($replace, '', $image_64); 
        $image = str_replace(' ', '+', $image); 
        $imageName = $name.Str::random(5).'.'.$extension;
        $user->photo = 'http://10.28.86.195:8000/storage/employee/'.$imageName;
        $user->save();

        Storage::disk('local')->put($imageName, base64_decode($image));

    }

}
