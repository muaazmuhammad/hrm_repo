<?php

namespace App\Http\Controllers\api;

use App\User;
use App\Resign;
use App\Mail\test;
use App\Mail\terminate;
use App\Mail\noticeMail;
use App\Mail\notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class resignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function empAllResign()
    {

        $user = auth()->user();
        $resign = Resign::where('user_id',$user->id)->orderBy('created_at', 'desc')->get();     //remaining
        return response()->json($resign);
        //
    }


    public function showAllResigns()
    {

        $resigns = Resign::all();
        return response()->json($resigns);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {                                                   
        $this->validate($request, [
            'user_id' => 'required|integer',
            'comment' => 'required',
        ]);

        //if already resign 
        $alreadyResignCheck = Resign::where([['user_id',$request->user_id],['status','pending']])->get();  

        if( $alreadyResignCheck->isEmpty()){
            //create new resign 
            $resign = new Resign();
            $resign->user_id = $request->user_id;
            $resign->comment = $request->comment;
            if ($resign->save()){
                return response()->json(['success' => true,'resign' => $resign],201);
            }
            else {
                return response()->json([ 'success' => false,'message' => 'Sorry, resign could not be added, its already sent'], 500);
            }
        }
        else {
        return response()->json([
            'success' => false,
            'message' => 'you have already resign. Please wait for the response from Hr.'
        ], 200);
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {          
        $resign = Resign::find($id);
        $this->authorize('view', $resign);

        if (!$resign) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, resign with id ' . $id . ' cannot be found'
            ], 400);
        }
        return $resign;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $resign = Resign::find($id);
        $this->authorize('update', $resign);
        if (is_null($resign)){
            return response()->json("resign you want to update is not exist " , 404);
        }
        $resign->update($request->all());

        return response()->json(
            [   'success' => true,
                'message' => 'resign updated',
            ] , 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resign = Resign::find($id);
        // if(auth()->user()->id  === $resign->user_id  ){
        //     return 'ok';
        // }


        $this->authorize('delete', $resign);
        
        if (is_null($resign)){
            return response()->json("not found" , 404);
        }
        $resign->delete();
        return response()->json("item is deleted " , 200);
        //
    }
    

    public function approveResign(Resign $resign){

        // $resign = Resign::find($id);
        $user =User::find($resign->user->id);
        $user->is_resigned = '1';
        $user->end_date = $resign->updated_at;
        $resign->status ='approved';
        $resign->save();
        $user->save();
        //send emil to inform
        return response()->json("resign approved " , 200);
    }

    public function RejectResign(Resign $resign){

        // $resign = Resign::find($id);
        $user =User::find($resign->user->id);
      
        $resign->status ='rejected';
        $resign->save();
        $user->save();
        //send emil to inform
        return response()->json("resign rejected " , 200);
    }

    public function terminate(Request $request ){
        $this->validate($request, [
            'name' => 'required',
            'email'=>'required',
            'comment' => 'required',
            'user_id'=> 'required'
        ]);
        $data =$request->all();
        $employee =User::findorfail($request->user_id);
        
        $employee->is_terminated = '1';
        $employee->end_date =date('Y-m-d H:i:s');
        $employee->save();
        Mail::to($request->email)->send(new terminate($data));
        return response()->json("employee terminated and mail sent." , 200);
    }

    public function sendNoticeEmail(Request $request ){
  
        $this->validate($request, [
            'comment' => 'required',
            'subject'=>'required',
        ]);
        $data =$request->all();
         $emails=User::all()->pluck("email");
                foreach ($emails as $recipient) {
                    Mail::to($recipient)->send(new notification($data));
                }
        return response()->json("employee terminated and mail sent." , 200);
    }   
}



