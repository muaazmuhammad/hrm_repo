<?php

namespace App\Http\Controllers\api;

use App\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class departmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        return response()->json($departments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $this->validate($request, [
            'name' => 'required',
        ]);

        $department = new Department();
        $department->name = $request->name;
    
        if ($department->save()) {
            return response()->json([
                'success' => true,
                'department' => $department
            ],201);
        }
        else {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, department could not be added'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $department = Department::find($id);
        if (is_null($department)){
            return response()->json("department you want to update is not exist " , 404);
        }
        $department->update($request->all());
        return response()->json(
            [   'success' => true,
                'message' => 'department updated',
            ] , 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $department = Department::find($id);

        if (is_null($department)){
            return response()->json("not found" , 404);
        }
        $department->delete();
        return response()->json("item is deleted " , 200);
    }
}
