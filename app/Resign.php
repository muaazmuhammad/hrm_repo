<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Resign
 *
 * @property int $id
 * @property string $comment
 * @property int $user_id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Resign newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Resign newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Resign query()
 * @method static \Illuminate\Database\Eloquent\Builder|Resign whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resign whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resign whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resign whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resign whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Resign whereUserId($value)
 * @mixin \Eloquent
 */
class Resign extends Model
{
    //
    protected $fillable = ['comment'  , 'user_id' ,'status' ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
