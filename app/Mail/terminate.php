<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class terminate extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $employee;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->employee= $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    
    {
        
        return $this->view('mails.terminateEmployee')->with('employee');
    }
}
