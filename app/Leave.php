<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Leave
 *
 * @property int $id
 * @property string $comment
 * @property string $start_date
 * @property string $end_date
 * @property int $user_id
 * @property int $count
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Leave newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Leave newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Leave query()
 * @method static \Illuminate\Database\Eloquent\Builder|Leave whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Leave whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Leave whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Leave whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Leave whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Leave whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Leave whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Leave whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Leave whereUserId($value)
 * @mixin \Eloquent
 */
class Leave extends Model
{
    //
    protected $fillable = ['comment' , 'start_date' , 'end_date' , 'user_id' ,'status', 'type' ];

    public function user() //an employe
    {
        return $this->belongsTo('App\User');
    }
}
